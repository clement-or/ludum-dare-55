extends Node

@export var ingredient: Ingredient

func _ready():
	assert(ingredient and ingredient.ingredient_type != Ingredient.IngredientType.None, "Un component ingrédient n'a pas de ressource Ingredient ou de type d'ingrédient")
	owner = get_parent()
	get_parent().add_to_group("ingredient")
	emit(false)


func emit(val: bool):
	$Mesh.visible = val
	print("Emit " + str(val))
