extends Resource
class_name Ingredient

enum IngredientType {
	None = 0,
	Fungus = 2,
	Goat = 3,
	Mandrake = 5,
	GoatPiece = 7,
	PlantPiece = 13,
	FungusPiece = 17
}

@export var ingredient_type: IngredientType
@export var amount := 1
