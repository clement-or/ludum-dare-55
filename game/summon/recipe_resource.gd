extends Resource
class_name Recipe

@export var recipe: Array[Ingredient]
@export var result: PackedScene

func sum():
	var s = 0
	for ing in recipe:
		s += ing.ingredient_type
	return s
