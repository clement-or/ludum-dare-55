extends Area3D

const MAX_INGREDIENTS = 4

@export var recipes: Array[Recipe]
@export var default_recipe: Recipe

var _ingredients = []
var _pending_ingredients = []

var _summoning = false
var _ingredients_sum = 0

func _on_summon_requested():
	print("Summon requested")
	if _summoning: return

	_ingredients_sum = 0
	for ing in _ingredients:
		_ingredients_sum += ing.ingredient.ingredient_type
		ing.owner.queue_free()

	$SummoningFeedback.visible = true
	$SummoningFeedback/Particles.emitting = true
	$AttachedUI.visible = true
	$Timer.start()
	

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	$AttachedUI/Progress.value = 1.0 - $Timer.time_left / $Timer.wait_time



func _on_body_exited(body:Node3D) -> void:
	if not body.is_in_group("ingredient"): return

	var comp = body.get_node_or_null("Ingredient")
	if not comp: return

	if _ingredients.find(comp) > -1:
		_ingredients.remove_at(_ingredients.find(comp))
		var ing = _pending_ingredients.pop_back()
		if ing:
			_ingredients.append(ing)
			ing.emit(true)
		comp.emit(false)
		return 

	if _pending_ingredients.find(comp) > -1:
		_pending_ingredients.remove_at(_pending_ingredients.find(comp))

func _on_body_entered(body:Node3D) -> void:
	if not body.is_in_group("ingredient"): return

	var comp = body.get_node_or_null("Ingredient")
	print(comp)
	if not comp: return

	if _ingredients.size() >= MAX_INGREDIENTS and _pending_ingredients.find(comp) == -1:
		_pending_ingredients.append(comp)
		comp.emit(false)
		return

	if _ingredients.size() < MAX_INGREDIENTS and _ingredients.find(comp) == -1:
		_ingredients.append(comp)
		comp.emit(true)

func _on_timer_timeout():
	var recipe = null
	for r in recipes:
		if r.sum() == _ingredients_sum:
			recipe = r
			break

	if recipe == null:
		recipe = default_recipe

	if recipe.result:
		var i = recipe.result.instantiate()
		get_tree().root.add_child(i)
		i.position = $SpawnPos.global_position

	_summoning = false
		
	$SummoningFeedback.visible = false
	$SummoningFeedback/Particles.emitting = false
	$AttachedUI.visible = false
