extends Path3D
class_name Client

@export var sprite_front: Polygon2D
@export var sprite_back: Polygon2D
@export var textures_front: Array[Texture2D]
@export var textures_back: Array[Texture2D]

@onready var ui := $AttachedUI
@onready var visuals := $Visuals/Body
@onready var timer := $Timer
@export var request: Array[Request]
@export var respawn_delay_s: Vector2
@export var speed_mps: float

@export var spriteDemon : Node
@export var spriteGhost : Node
@export var spriteHappy : Node
@export var spriteSad : Node

enum ServeState {
	None,
	Happy,
	Sad
}

var _moving = false
var _reverse = false
var _sprite = 0
var requestName : String
var hasBeenServed = false
var serve_state = ServeState.None

func _ready():
	add_to_group("client")
	$Visuals/Body.owner = self
	visuals.visible = false
	ui.visible = false
	spriteSad.visible = false
	spriteHappy.visible = false
	start_time()

func _process(delta: float) -> void:
	if _moving:
		if !_reverse:
			$Visuals.progress += speed_mps * delta
		else:
			$Visuals.progress -= speed_mps * delta
	if $Visuals.progress_ratio >= 0.99 and $WaitTimer.is_stopped():
		_moving = false
		generate_request()
		$WaitTimer.start()
		print("OK")
	if $Visuals.progress <= 0 and _reverse:
		visuals.visible = false
		_moving = false
		_reverse = false
		start_time()
	match serve_state:
		ServeState.None:
			spriteHappy.visible = false
			spriteSad.visible = false
		ServeState.Happy:
			spriteHappy.visible = true
			spriteSad.visible = false
		ServeState.Sad:
			spriteHappy.visible = false
			spriteSad.visible = true

	ui.get_node("Panel/Progress").value = 1.0 - $WaitTimer.time_left / $WaitTimer.wait_time

func start_time():
	var rnd = randf_range(respawn_delay_s.x, respawn_delay_s.y)
	timer.wait_time = rnd
	timer.start()

func generate_request():
	ui.visible = true
	sprite_back.visible = true
	sprite_front.visible = false
	requestName = request.pick_random().group_name
	if requestName == "demon":
		spriteDemon.visible = true
	elif requestName == "ghost":
		spriteGhost.visible = true
		
func _leave():
	ui.visible = false
	sprite_back.visible = false
	sprite_front.visible = true
	_moving = true
	_reverse = true
	$WaitTimer.stop()

func _on_timer_timeout() -> void:
	visuals.visible = true
	_moving = true
	sprite_back.visible = false
	sprite_front.visible = true
	ui.visible = false
	var rnd = randi_range(0,2)
	sprite_back.texture = textures_back[rnd]
	sprite_front.texture = textures_front[rnd]
	
func _on_wait_timer_timeout() -> void:
	serve_state = ServeState.Sad
	_leave()

func _on_item_targeted_by_use(used):
	print(requestName)
	print(str(used.owner.get_groups()))
	if used.owner.is_in_group("demon") or used.owner.is_in_group("ghost"):
		get_node("%Player").inventory.try_drop()
		used.owner.queue_free()
	
	if requestName == "demon" and used.owner.is_in_group("demon") or requestName == "ghost" and used.owner.is_in_group("ghost"):
		Score.score += 1
		serve_state = ServeState.Happy
		_leave()

		
