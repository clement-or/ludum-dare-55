extends Node

var camera: PhantomCamera3D

func _ready():
	if get_parent() is PhantomCamera3D:
		camera = get_parent()

func _on_garden_trigger_body_exited(body:Node3D) -> void:
	if not camera or not body.is_in_group("player"): return 
	camera.set_priority(0)

func _on_garden_trigger_body_entered(body:Node3D) -> void:
	if not camera or not body.is_in_group("player"): return 
	camera.set_priority(2)

