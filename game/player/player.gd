extends CharacterBody3D

signal interaction_pending(action: StringName, text: String, progress: float)
signal summon_requested()

@onready var sm = $StateMachinePlayer
@onready var anim = $animFinales
@onready var eyes_anim = $EyesAnim
@onready var parent_anim = $ParentAnim
@onready var grab_area = $GrabArea
@onready var inventory := $Inventory
#@onready var grabbed_position := $GrabbedPosition
@onready var raycast := $Raycast
@onready var placement_feedback := $PlacementFeedback
@onready var hold_timer := $HoldTimer
@onready var playerSpriteRoot : Node = $playerSprites_root

@export var grabbed_position : RemoteTransform3D
@export var walk_speed_mps: float
@export var damping: float
@export var drop_distance_m: float

var move_input = Vector3(0,0,0)
var direction = Vector3.ZERO

var _previous_velocity = Vector3(0,0,0)
var _target_item: Node3D
var _main_camera: Camera3D
var _can_summon = false
var _object_in_hands = false

func _ready():
	assert(sm, "La StateMachine enfant du joueur est null, vérifiez qu'il y a un Node StateMachinePlayer enfant de Player")
	if walk_speed_mps <= 0:
		push_warning("Attention, le joueur a une vitesse de marche de zéro")

func _physics_process(delta: float) -> void:
	move_and_slide()
	velocity *= 1.0/damping

func _on_state_exited(from:Variant) -> void:
	pass

func _on_state_entered(to:Variant) -> void:
	pass

func _on_state_transition(from:Variant, to:Variant) -> void:
	if from == "Summon":
		hold_timer.stop()
	if to == "Summon":
		hold_timer.start()

func _on_state_updated(state:Variant, delta:Variant) -> void:
	move_input.x = int(Input.is_action_pressed("right")) - int(Input.is_action_pressed("left"))
	move_input.z = int(Input.is_action_pressed("down")) - int(Input.is_action_pressed("up"))
	sm.set_param("is_moving", move_input.length_squared() != 0)

	if move_input.x > 0:
		playerSpriteRoot.scale.x = -0.5
	elif move_input.x < 0:
		playerSpriteRoot.scale.x = 0.5
	
	direction = move_input.normalized() if move_input.length() > 0 else direction
	grab_area.direction = move_input
	raycast.target_position = direction * drop_distance_m

	if placement_feedback and raycast:
		placement_feedback.modulate = Color.RED if raycast.is_colliding() else Color.WHITE
	var oldy = placement_feedback.position.y
	placement_feedback.position = raycast.target_position * drop_distance_m
	placement_feedback.position.y = oldy
		
	if state != "Summon":
		if Input.is_action_just_pressed("drop"):
			if inventory and grab_area and inventory.content:
				if inventory.content:
					if not raycast.is_colliding():
						inventory.try_drop()
					else:
						inventory.fail_drop(Inventory.DropFailReason.NoRoom)

		if Input.is_action_just_pressed("action") and inventory:
			if not inventory.content and grab_area.preferred_item:
				inventory.try_pickup(grab_area.preferred_item)
			elif inventory and inventory.content:
				inventory.try_use(inventory.content.owner, grab_area.preferred_item)

		if _can_summon and Input.is_action_just_pressed("summon"):
			sm.set_param("summon", true)

		# Interaction prompts
		if not inventory.content and grab_area.preferred_item and not grab_area.preferred_item.is_in_group("client"):
			interaction_pending.emit("action", "Grab", 1.0)
		if inventory.content and grab_area.preferred_item:
			interaction_pending.emit("action", inventory.content.use_text, 1.0)
		if _can_summon:
			interaction_pending.emit("summon", "Summon", 1.0)

	match state:
		"Idle":
			if _object_in_hands:
				anim.play("Idle_Carry")
			else:
				anim.play("Idle")
		"Walk":
			if _object_in_hands:
				anim.play("Walk_Carry")
			else:
				anim.play("Walk")
			if move_input.x > 0:
				eyes_anim.play("right")
			elif move_input.x < 0:
				eyes_anim.play("left")
			if move_input.z > 0:
				eyes_anim.play("down")
			elif move_input.z < 0:
				eyes_anim.play("up")
			velocity += move_input.normalized() * walk_speed_mps
		"Summon":
			interaction_pending.emit("summon", "Summon",  1.0 - hold_timer.time_left / hold_timer.wait_time)
			if Input.is_action_just_released("summon"):
				sm.set_param("summon", false)


func _on_inventory_item_drop_failed(item:Item, reason:Inventory.DropFailReason) -> void:
	parent_anim.play("shake")


func _on_inventory_item_dropped(item:Item) -> void:
	grabbed_position.update_position = false
	item.owner.position = position + direction * drop_distance_m
	item.owner.visible = true
	_object_in_hands = false
	item.drop(self)
	if placement_feedback:
		placement_feedback.visible = false


func _on_inventory_item_used(item:Item, target:Item) -> void:
	print("USED " + str(item.owner.name) + " ON " + str(target.owner.name)) 
	pass # Replace with function body.


func _on_inventory_item_use_failed(item:Item) -> void:
	pass # Replace with function body.


func _on_inventory_item_pickup_failed(item:Item, reason:Inventory.PickupFailReason) -> void:
	pass # Replace with function body.

func _on_inventory_item_picked(item:Item) -> void:
	if item.is_in_group("summon_circle"):
		return
	if item.is_in_group("equipment"):
		item.owner.visible = false
		return
	else:
		if not item.owner.is_in_group("client"):
			grabbed_position.remote_path = item.owner.get_path()
			grabbed_position.update_position = true
	if placement_feedback:
		placement_feedback.visible = true
	_object_in_hands = true
	item.pickup(self)



func _on_preferred_grab_item_changed(node:Node3D, previous:Node3D) -> void:
	if node:
		var feedback = node.get_node_or_null("InteractableFeedback")
		if feedback and feedback is InteractableFeedback:
			feedback.highlight(true)
		_target_item = node

	if previous: 
		var feedback = previous.get_node_or_null("InteractableFeedback")
		if feedback and feedback is InteractableFeedback:
			feedback.highlight(false)



func _on_summon_circle_entered(node:Area3D) -> void:
	if node.is_in_group("summon_circle"):
		summon_requested.connect(node._on_summon_requested)
		_can_summon = true


func _on_area_exited(node:Area3D) -> void:
	if node.is_in_group("summon_circle"):
		summon_requested.disconnect(node._on_summon_requested)
		_can_summon = false



func _on_hold_timer_timeout() -> void:
	match sm.current:
		"Summon":
			sm.set_param("summoning", false)
			summon_requested.emit()
		

