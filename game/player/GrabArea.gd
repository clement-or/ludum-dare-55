extends Area3D

signal preferred_item_changed(node: Node3D, previous: Node3D)
signal summon_circle_entered(node: Area3D)
signal summon_circle_exited()

var direction = Vector3.ZERO
var _prev_owner_position := Vector3.ZERO
var preferred_item : Node3D

var _min_dist = INF
var _max_dot = -1

func _ready():
	owner = get_parent()
	_prev_owner_position = owner.global_position

func _physics_process(_delta: float):
	direction = (owner.global_position - _prev_owner_position).normalized()

	var prev_item = preferred_item
	preferred_item = null

	for area in get_overlapping_areas():
		pass

	_max_dot = -1
	_min_dist = INF
	for node in get_overlapping_bodies():
		if node == owner: continue
		if not node.is_in_group("item"): continue
		if not preferred_item: preferred_item = node

		var dist = owner.global_position.distance_to(node.global_position)
		var player_object_vec = node.global_position - owner.global_position
		var dot = direction.dot(player_object_vec)

		# Si la direction et la distance sont toutes les deux plus proches, alors choisir cet objet
		if dot >= _max_dot and dist <= _min_dist:
			preferred_item = node 
			_max_dot = dot 
			_min_dist = dist 
			continue

		# Si la distance est bonne, mais pas la direction, on prend quand même
		if dist <= _min_dist and dot > _max_dot:
			_min_dist = dist
			preferred_item = node 
			continue

	if prev_item != preferred_item:
		preferred_item_changed.emit(preferred_item, prev_item)


func _on_area_entered(area:Area3D) -> void:
	if area.is_in_group("summon_circle"):
		summon_circle_entered.emit(area)

func _on_area_exited(area:Area3D) -> void:
	if area.is_in_group("summon_circle"):
		summon_circle_exited.emit()
