extends Node3D

@onready var animTree : AnimationTree = $AnimationTree
@onready var eatClass : eatManager = $eat
@export var numberOfPieces : int = 3
var champiPieceObj = preload("res://NPC/champi/champi_piece.tscn")

func _process(delta):
	_champAttackAnimation()

func _champAttackAnimation():
	if eatClass.isSleeping:
		animTree["parameters/conditions/attack"] = true
		animTree["parameters/conditions/endSleep"] = false
	else:
		animTree["parameters/conditions/endSleep"] = true
		animTree["parameters/conditions/attack"] = false
		$stun_fx.visible = false

func _customDestroy():
	var rng = RandomNumberGenerator.new()
	for i in numberOfPieces:
		var pieceInstance = champiPieceObj.instantiate()
		var xRand = rng.randf_range(-1, 1)
		var zRand = rng.randf_range(-1, 1)
		get_tree().root.add_child(pieceInstance)
		pieceInstance.global_position = Vector3(global_position.x + xRand, global_position.y, global_position.z + zRand)
		
	queue_free()

func _on_item_targeted_by_use(used):
	if used.owner.is_in_group("cleaver"):
		_customDestroy()


func _on_item_picked(by):
	scale = Vector3(0.5,0.5,0.5)
	$CollisionShape3D.disabled = true


func _on_item_dropped(by):
	scale = Vector3(1,1,1)
	$CollisionShape3D.disabled = false
