extends Node

@export var moveComp : IA_MoveComp
@export var idleTimerBase : float = 2.0
var idleTimer = idleTimerBase
var targets : Array[Node]
var state = "idle"
@export var animTree : AnimationTree
@export var col : CollisionShape3D
var pickedUp = false

func _ready():
	targets = get_tree().get_nodes_in_group("MarkerTargetDemon")
	_chooseTarget()

func _process(delta):
	if !pickedUp:
		if state == "idle":
			if idleTimer >= 0:
				idleTimer -= delta		
			else:
				_enterMoveState()
		elif state == "move":
			if (moveComp.destinationReached == true):
				_enterIdleState()

func _chooseTarget():
	if (targets.size() > 0):
		moveComp.moveTarget = targets.pick_random().global_position

func _enterMoveState():
	state = "move"
	moveComp.process_mode = 0
	_chooseTarget()
	moveComp.destinationReached = false
	
func _enterIdleState():
	state = "idle"
	idleTimer = idleTimerBase
	moveComp.process_mode = 4


func _on_item_picked(by):
	col.disabled = true
	pickedUp = true


func _on_item_dropped(by):
	col.disabled = false
	pickedUp = false
