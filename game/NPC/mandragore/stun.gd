class_name stunClass extends Node3D

@export var stunTimer : float = 5.0
@export var behaviorNodes : Node
@export var eatNode : Node
var timer : float = stunTimer
@export var animTree : AnimationTree

func _process(delta):
	if timer >= 0:
		timer -= delta
	else:
		_endStun()
		

func _startStun():
	timer = stunTimer
	process_mode = 0
	behaviorNodes.process_mode = 4
	if eatNode != null:
		eatNode.process_mode = 4
	if (animTree != null):
		animTree["parameters/conditions/endStun"] = false
		animTree["parameters/conditions/stun"] = true
	$stun_fx.visible = true

func _endStun():
	behaviorNodes.process_mode = 0
	if eatNode != null:
		eatNode.process_mode = 0
	process_mode = 4
	if (animTree != null):
		animTree["parameters/conditions/endStun"] = true
		animTree["parameters/conditions/stun"] = false
	$stun_fx.visible = false
	
