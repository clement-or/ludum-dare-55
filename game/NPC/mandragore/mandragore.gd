class_name mandragore extends CharacterBody3D

@onready var behaviorNodes : Node3D = $BehaviorNodes
var mandragorePieceObj = preload("res://NPC/mandragore/mandragore_piece.tscn")
@export var numberOfPieces : int = 3
@onready var animTree : AnimationTree = $AnimationTree
@onready var stun : stunClass = $stun

func _stunByGoat():
	if behaviorNodes != null:
		behaviorNodes.process_mode = 4
	
func _resetBehavior():
	if behaviorNodes != null:
		behaviorNodes.process_mode = 0
		
func _stun():
	stun._startStun()

func _customDestroy():
	var rng = RandomNumberGenerator.new()
	for i in numberOfPieces:
		var pieceInstance = mandragorePieceObj.instantiate()
		var xRand = rng.randf_range(-1, 1)
		var zRand = rng.randf_range(-1, 1)
		get_tree().root.add_child(pieceInstance)
		pieceInstance.global_position = Vector3(global_position.x + xRand, global_position.y, global_position.z + zRand)
		
	queue_free()


func _on_item_targeted_by_use(used):
	if used.owner.is_in_group("cleaver"):
		_customDestroy()
	if used.owner.is_in_group("mortar"):
		_stun()


func _on_item_picked(by):
	$CollisionShape3D.disabled = true


func _on_item_dropped(by):
	$CollisionShape3D.disabled = false
