extends Node3D

@export var moveComp : IA_MoveComp
var targets : Array[Node]

func _ready():
	targets = get_tree().get_nodes_in_group("MarkerTarget")
	_chooseTarget()

func _process(delta):
	if (moveComp.destinationReached == true):
		_chooseTarget()
		moveComp.destinationReached = false

func _chooseTarget():
	if (targets.size() > 0):
		moveComp.moveTarget = targets.pick_random().global_position
