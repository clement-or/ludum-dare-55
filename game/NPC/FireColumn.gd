class_name fireColumn extends Node3D

@export var particleSystem : GPUParticles3D
@export var col : Area3D
var ignited = false
var igniteTimer : float = 0.5
var fireTimer : float = 1.5

func _ready():
	particleSystem.emitting = false
	col.monitoring = false

func _process(delta):
	if ignited:
		if igniteTimer >= 0:
			igniteTimer -= delta
		else:
			_activeFire()
			if fireTimer >= 0:
				fireTimer -= delta
			else:
				_deactivateFire()

func _activeFire():
	if !particleSystem.emitting:
		particleSystem.emitting = true
	if col.monitoring == false:
		col.monitoring = true

func _deactivateFire():
	particleSystem.emitting = false
	col.monitoring = false
	igniteTimer = 0.5
	fireTimer = 1.5
	ignited = false

func _on_area_3d_body_entered(body):
	if body.is_in_group("ingredient"):
		body.queue_free()
