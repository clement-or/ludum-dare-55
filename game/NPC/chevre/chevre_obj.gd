class_name chevre extends CharacterBody3D

@onready var behaviorNodes = $behavior
@export var numberOfPieces : int = 3
var meatPieceObj = preload("res://NPC/chevre/meat_piece.tscn")
@onready var eatScript : eatManager = $eat
@onready var animTree : AnimationTree = $AnimationTree
@onready var stun : stunClass = $stun

func _process(delta):
	_updateAnimationParameters()

func _enterEatState(eatenMandragor : Node):
	var eatChild = $eat
	var behaviorChild = $behavior
	behaviorChild.process_mode = 4
	eatChild.process_mode = 0
	eatScript.objectToDestroy = eatenMandragor

func _stun():
	stun._startStun()
	eatScript._resetProcessMandragore()
	
	
func _customDestroy(spawnMeat : bool):
	if spawnMeat:
		var rng = RandomNumberGenerator.new()
		for i in numberOfPieces:
			var pieceInstance = meatPieceObj.instantiate()
			var xRand = rng.randf_range(-1, 1)
			var zRand = rng.randf_range(-1, 1)
			get_tree().root.add_child(pieceInstance)
			pieceInstance.global_position = Vector3(global_position.x + xRand, global_position.y, global_position.z + zRand)
	
	eatScript._resetProcessMandragore()
	queue_free()
	
func _updateAnimationParameters():
	if eatScript.isEating:
		animTree["parameters/conditions/eating"] = true
		animTree["parameters/conditions/not_eating"] = false
	else:
		animTree["parameters/conditions/eating"] = false
		animTree["parameters/conditions/not_eating"] = true


func _on_item_targeted_by_use(used):
	if (used.owner.is_in_group("cleaver")):
		_customDestroy(true)
	if (used.owner.is_in_group("mortar")):
		_stun()
		

func _on_item_picked(by):
	scale = Vector3(0.3,0.3,0.3)
	$CollisionShape3D.disabled = true
	eatScript._resetProcessMandragore()


func _on_item_dropped(by):
	scale = Vector3(0.5,0.5,0.5)
	$CollisionShape3D.disabled = false
