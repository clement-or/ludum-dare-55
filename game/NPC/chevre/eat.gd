class_name eatManager extends Node3D

@export var eatMainTimer : float = 2.0
@export var eatMeat : bool = false
@export var sleepTime : float = 10.0
@export var idleSprite : Node
@export var sleepSprite : Node
var timer : float = eatMainTimer
var sleepTimer : float = sleepTime
var mandragoreToDestroy : mandragore
var pieceToDestroy : Node
var chevreToDestroy : chevre
var behavior
var isEating = false
var isSleeping = false

func _ready():
	behavior = get_node("../behavior")

func _process(delta):
	if isEating:
		if (timer >= 0):
			timer -= delta
		else:
			if !eatMeat:
				if mandragoreToDestroy != null:
					mandragoreToDestroy._customDestroy()
					_endEat()
				elif pieceToDestroy != null:
					pieceToDestroy.queue_free()
					_endEat()
	elif isSleeping:
		if (sleepTimer >= 0):
			sleepTimer -= delta
		else:
			sleepTimer = sleepTime
			isSleeping = false

func _endEat():
	behavior.process_mode = 0
	timer = eatMainTimer
	isEating = false

func _on_area_3d_body_entered(body):
	if !isEating:
		if !eatMeat:
			if body.is_in_group("Mandragore"):
				timer = eatMainTimer
				isEating = true
				behavior.process_mode = 4
				mandragoreToDestroy = body
				mandragoreToDestroy.behaviorNodes.process_mode = 4
				mandragoreToDestroy.animTree["parameters/conditions/eaten"] = true
				body.global_position = get_node("../Sprite3D/eatPos").global_position
			elif body.is_in_group("MandragorePiece"):
				timer = eatMainTimer
				isEating = true
				behavior.process_mode = 4
				pieceToDestroy = body
		else:
			if body.is_in_group("Chevre"):
				chevreToDestroy = body
				chevreToDestroy._customDestroy(false)
				isSleeping = true
				sleepTimer = sleepTime
				if body.global_position.x > get_parent().global_position.x:
					get_node("../Sprite3D2").scale.x = -0.6
				else:
					get_node("../Sprite3D2").scale.x = 0.6
			elif body.is_in_group("MeatPiece"):
				pieceToDestroy = body
				pieceToDestroy.queue_free()
				isSleeping = true
				sleepTimer = sleepTime
				if body.global_position.x > get_parent().global_position.x:
					get_node("../Sprite3D2").scale.x = -0.6
				else:
					get_node("../Sprite3D2").scale.x = 0.6
				
func _resetProcessMandragore():
	if mandragoreToDestroy != null:
		mandragoreToDestroy.behaviorNodes.process_mode = 0
