class_name chevreTargetManager extends Node3D

@export var moveComp : IA_MoveComp
@export var distToStunMandragore : float = 0.5
var targets : Array[Node]
var secondaryTargets : Array[Node]
var mandragoreToEat : Node

func _ready():
	
	_findMandragoresTargets()
	
	#if (secondaryTargetParent != null):
	#	for i in secondaryTargetParent.get_children():
	#		secondaryTargets.append(i)
	secondaryTargets = get_tree().get_nodes_in_group("MarkerTarget")
	
	_chooseTarget()

func _process(delta):		
	if (moveComp.destinationReached == true):
		_chooseTarget()
		moveComp.destinationReached = false

func _chooseTarget():
	_findMandragoresTargets()
	if (targets.size() > 0):
		mandragoreToEat = targets.pick_random()
		moveComp.moveTarget = mandragoreToEat.global_position
	elif (secondaryTargets.size() > 0):
		moveComp.moveTarget = secondaryTargets.pick_random().global_position

func _findMandragoresTargets():
	targets.clear()
	targets = get_tree().get_nodes_in_group("Mandragore") + get_tree().get_nodes_in_group("MandragorePiece")
