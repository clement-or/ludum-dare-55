class_name IA_MoveComp extends Node3D

@export var speed = 1
@export var accel = 10
@export var body : CharacterBody3D
@onready var nav : NavigationAgent3D = $NavigationAgent3D
@export var moveTarget : Vector3
@export var distanceToTarget = 2.0
@export var spriteRoot : Node
var destinationReached = false

func _physics_process(delta):
	var direction = Vector3()
	
	if moveTarget != null:
		nav.target_position = moveTarget
		if (global_position - moveTarget).length() > distanceToTarget:
			direction = nav.get_next_path_position() - global_position
			direction = direction.normalized()
		
			body.velocity = body.velocity.lerp(direction*speed, accel*delta)
		
			body.move_and_slide()
			
			if (spriteRoot != null):
				if (body.velocity.x >= 0):
					spriteRoot.scale.x = -1
				else:
					spriteRoot.scale.x = 1
		else:
			destinationReached = true
