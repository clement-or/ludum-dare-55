extends Node3D

@export var moveComp : IA_MoveComp
@export var fireTimerBase : float = 6.0
@export var fireResetTimerBase : float = 1.7
@export var idleTimerBase : float = 1.0
@export var col : CollisionShape3D
var fireTimer = fireTimerBase
var resetFireTimer = fireResetTimerBase
var idleTimer = idleTimerBase
var targets : Array[Node]
var state = "idle"
@export var animTree : AnimationTree
var fireColumnNodes : Array[Node]
var fireColum : Array[fireColumn]
var pickedUp = false

func _ready():
	targets = get_tree().get_nodes_in_group("MarkerTargetDemon")
	_chooseTarget()
	fireColumnNodes = get_tree().get_nodes_in_group("fireColumn")
	for i in fireColumnNodes.size():
		fireColum.append(fireColumnNodes[i])
		

func _process(delta):
	if !pickedUp:
		if state == "callFire":
			if fireResetTimerBase >= 0:
				fireResetTimerBase -= delta
			else:
				_enterIdleState()
			
		else:
			if state == "idle":
				if idleTimer >= 0:
					idleTimer -= delta		
				else:
					_enterMoveState()
			elif state == "move":
				if (moveComp.destinationReached == true):
					_enterIdleState()
			if fireTimer >= 0:
				fireTimer -= delta
			else:
				_enterCallFireState()


func _chooseTarget():
	if (targets.size() > 0):
		moveComp.moveTarget = targets.pick_random().global_position

func _enterMoveState():
	state = "move"
	moveComp.process_mode = 0
	_chooseTarget()
	moveComp.destinationReached = false
	animTree["parameters/conditions/idle"] = false
	animTree["parameters/conditions/callFire"] = false
	animTree["parameters/conditions/move"] = true
	
func _enterIdleState():
	state = "idle"
	idleTimer = idleTimerBase
	resetFireTimer = fireResetTimerBase
	moveComp.process_mode = 4
	animTree["parameters/conditions/idle"] = true
	animTree["parameters/conditions/callFire"] = false
	animTree["parameters/conditions/move"] = false
	
func _enterCallFireState():
	state = "callFire"
	fireTimer = fireTimerBase
	moveComp.process_mode = 4
	animTree["parameters/conditions/idle"] = false
	animTree["parameters/conditions/callFire"] = true
	animTree["parameters/conditions/move"] = false
	for i in 5:
		var fire : fireColumn
		fire = fireColum.pick_random()
		fire.ignited = true


func _on_item_dropped(by):
	col.disabled = false
	pickedUp = false


func _on_item_picked(by):
	col.disabled = true
	pickedUp = true
