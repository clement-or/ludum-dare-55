@tool
extends Sprite3D

func _process(_delta):
	# Retrieve the captured Image using get_image().
	var img = $Viewport.get_texture().get_image()
	# Convert Image to ImageTexture.
	var tex = ImageTexture.create_from_image(img)
	# Set sprite texture.
	texture = tex
