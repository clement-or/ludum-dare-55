extends Node
class_name Inventory

enum PickupFailReason {
	None,
	InventoryFull
}

enum DropFailReason {
	None,
	NoRoom,
	NoItemToDrop
}
signal item_picked(item: Item)																		# Un item vient d'être attrapé
signal item_pickup_failed(item: Item, reason: PickupFailReason)		# Un item n'a pas pu être attrapé

signal item_dropped(item: Item)																		# Un item a été déposé/lâché
signal item_drop_failed(item: Item, reason: DropFailReason)				# Un item n'a pas pu être lâché

signal item_used(used: Item, target: Item)
signal item_use_failed(item: Item)

var content: Item

func try_use(used: Node, target: Node) -> void:
	var used_item = get_item(used)
	var target_item = get_item(target)
	if used_item and target_item:
		used_item.use(target_item)
		target_item.use_target(used_item)
		item_used.emit(used_item, target_item)

func try_pickup(node: Node) -> void:
	var item = get_item(node)
	if not item: return
	if content:
		item_pickup_failed.emit(item, PickupFailReason.InventoryFull)
		return
	else:
		content = item
		item_picked.emit(item)
	
func try_drop() -> void:
	if not content:
		item_drop_failed.emit(content, DropFailReason.NoItemToDrop)
		return
	content.drop(self)
	item_dropped.emit(content)
	content = null

func fail_drop(reason: DropFailReason):
	item_drop_failed.emit(content, reason)
	
# Essaie de récupérer le component Item sur n'importe quel node
func get_item(node: Node) -> Item:
	if not node: return null
	var maybe_item = node.get_node_or_null("Item")
	if maybe_item: return maybe_item

	return null
