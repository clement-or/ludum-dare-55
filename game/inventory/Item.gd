extends Node3D
class_name Item

signal picked(by: Node)
signal dropped(by: Node)
signal used(target: Item)
signal targeted_by_use(used: Item)

@export var use_text = "Use"

@export var grabbable = true

func _ready():
	owner = get_parent()
	owner.set_meta("ItemComponent", self)
	owner.add_to_group("item")

func pickup(by: Node):
	picked.emit(by)

func drop(by: Node):
	dropped.emit(by)

func use(target: Item):
	used.emit(target)

func use_target(other: Item):
	targeted_by_use.emit(other)
