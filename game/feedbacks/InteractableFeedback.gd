extends Sprite3D
class_name InteractableFeedback

func _ready():
	owner = get_parent()
	owner.set_meta("InteractableFeedbackComponent", self)

func highlight(state := true):
	modulate = Color.from_string("#f7a72a", Color.LIGHT_YELLOW) if state else Color.WHITE
