extends VBoxContainer

var _to_display = []

func _on_interaction_pending(action:StringName, text:String, progress:float) -> void:
	for c in get_children():
		if c.action_name == action:
			c.action_text = text
			c.progress = progress
			_to_display.append(c)


func _process(_delta):
	for i in get_children():
		i.visible = false
	for i in _to_display:
		i.visible = true
	_to_display = []
			

