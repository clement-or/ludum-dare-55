@tool
extends Control
class_name AttachedUI

@export var offset: Vector3
@export var remote_transform: Node3D

func _ready():
	owner = get_parent()

func _physics_process(_delta: float):
	if not owner: return
	var cam = get_viewport().get_camera_3d()
	if not cam: return
	var object_position = owner.global_position + offset
	if remote_transform:
		object_position = remote_transform.global_position
	var screen_position = cam.unproject_position(object_position)

	position = screen_position

	
