@tool
extends HBoxContainer

@export var progress: float
@export var action_name: StringName
@export var action_text: String

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	$ActionIcon.action_name = action_name
	$Label.text = action_text
	$Progress.value = progress

	$Progress.visible = progress != 1.0
	$ActionIcon.visible = progress == 1.0


	if not action_name in InputMap.get_actions(): return

	if Input.is_action_pressed(action_name):
		$AnimationPlayer.play("pressed")
	if Input.is_action_just_released(action_name):
		$AnimationPlayer.play("prompt")
