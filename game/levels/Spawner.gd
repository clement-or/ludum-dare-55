extends Node3D

var chevreObj : Array[Node]
var champiObj : Array[Node]
var mandragoreObj : Array[Node]

var chevrePrefab = preload("res://NPC/chevre_obj.tscn")
var champiPrefab = preload("res://NPC/champi/champi_obj.tscn")
var mandragorePrefab = preload("res://NPC/mandragore/mandragore_ph.tscn")

@export var chevreSpawnerPos : Node
@export var champiSpawnerPos : Node
@export var mandragoreSpawnerPos : Node
@export var baseTime : float = 5

var timer = baseTime

func _process(delta):
	
	if timer >= 0:
		timer -= delta
	else:
		chevreObj = get_tree().get_nodes_in_group("Chevre")
		champiObj = get_tree().get_nodes_in_group("Champi")
		mandragoreObj = get_tree().get_nodes_in_group("Mandragore")
		
		if (chevreObj.size() < 2):
			var chevreInstance = chevrePrefab.instantiate()
			get_tree().root.add_child(chevreInstance)
			chevreInstance.global_position = chevreSpawnerPos.global_position
		if (champiObj.size() < 1):
			var champiInstance = champiPrefab.instantiate()
			get_tree().root.add_child(champiInstance)
			champiInstance.global_position = champiSpawnerPos.global_position
		if (mandragoreObj.size() < 4):
			var mandInstance = mandragorePrefab.instantiate()
			get_tree().root.add_child(mandInstance)
			mandInstance.global_position = mandragoreSpawnerPos.global_position
			
		timer = baseTime
	
	
